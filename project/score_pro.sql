-- 班级表
CREATE TABLE class (
    class_id INT PRIMARY KEY AUTO_INCREMENT,
    class_code VARCHAR(20) NOT NULL COMMENT '班级编号',
    class_name VARCHAR(50) NOT NULL COMMENT '班级名称',
    teacher_name VARCHAR(20) NOT NULL COMMENT '班主任姓名',
    student_count INT DEFAULT 0 COMMENT '学生人数',
    description TEXT COMMENT '班级描述',
    create_time DATETIME DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
    update_time DATETIME DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
    UNIQUE KEY uk_class_code (class_code)
) COMMENT '班级信息表';

-- 学生表
CREATE TABLE student (
    student_id INT PRIMARY KEY AUTO_INCREMENT,
    student_code VARCHAR(20) NOT NULL COMMENT '学号',
    name VARCHAR(20) NOT NULL COMMENT '姓名',
    gender CHAR(1) NOT NULL COMMENT '性别：M-男，F-女',
    class_id INT NOT NULL COMMENT '所属班级ID',
    phone VARCHAR(20) COMMENT '联系电话',
    enroll_date DATE NOT NULL COMMENT '入学时间',
    status TINYINT DEFAULT 1 COMMENT '状态：1-在读，2-毕业，3-退学',
    create_time DATETIME DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
    update_time DATETIME DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
    UNIQUE KEY uk_student_code (student_code),
    FOREIGN KEY (class_id) REFERENCES class(class_id)
) COMMENT '学生信息表';

-- 科目表
CREATE TABLE subject (
    subject_id INT PRIMARY KEY AUTO_INCREMENT,
    subject_code VARCHAR(20) NOT NULL COMMENT '科目编号',
    subject_name VARCHAR(50) NOT NULL COMMENT '科目名称',
    description TEXT COMMENT '科目描述',
    create_time DATETIME DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
    update_time DATETIME DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
    UNIQUE KEY uk_subject_code (subject_code)
) COMMENT '科目信息表';

-- 考试表
CREATE TABLE exam (
    exam_id INT PRIMARY KEY AUTO_INCREMENT,
    exam_name VARCHAR(100) NOT NULL COMMENT '考试名称',
    exam_date DATE NOT NULL COMMENT '考试日期',
    description TEXT COMMENT '考试描述',
    status TINYINT DEFAULT 1 COMMENT '状态：1-未开始，2-进行中，3-已结束',
    create_time DATETIME DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
    update_time DATETIME DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间'
) COMMENT '考试信息表';

-- 成绩表
CREATE TABLE grade (
    grade_id INT PRIMARY KEY AUTO_INCREMENT,
    student_id INT NOT NULL COMMENT '学生ID',
    subject_id INT NOT NULL COMMENT '科目ID',
    exam_id INT NOT NULL COMMENT '考试ID',
    score DECIMAL(5,2) NOT NULL COMMENT '分数',
    rank_in_class INT COMMENT '班级排名',
    rank_in_grade INT COMMENT '年级排名',
    remark TEXT COMMENT '备注',
    create_time DATETIME DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
    update_time DATETIME DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
    FOREIGN KEY (student_id) REFERENCES student(student_id),
    FOREIGN KEY (subject_id) REFERENCES subject(subject_id),
    FOREIGN KEY (exam_id) REFERENCES exam(exam_id),
    UNIQUE KEY uk_student_subject_exam (student_id, subject_id, exam_id)
) COMMENT '成绩信息表';

-- 教师表
CREATE TABLE teacher (
    teacher_id INT PRIMARY KEY AUTO_INCREMENT,
    teacher_code VARCHAR(20) NOT NULL COMMENT '教师编号',
    name VARCHAR(20) NOT NULL COMMENT '姓名',
    gender CHAR(1) NOT NULL COMMENT '性别：M-男，F-女',
    phone VARCHAR(20) COMMENT '联系电话',
    email VARCHAR(50) COMMENT '邮箱',
    status TINYINT DEFAULT 1 COMMENT '状态：1-在职，2-离职',
    create_time DATETIME DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
    update_time DATETIME DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
    UNIQUE KEY uk_teacher_code (teacher_code)
) COMMENT '教师信息表';

-- 用户表
CREATE TABLE user (
    user_id INT PRIMARY KEY AUTO_INCREMENT,
    username VARCHAR(50) NOT NULL COMMENT '用户名',
    password VARCHAR(100) NOT NULL COMMENT '密码',
    role TINYINT NOT NULL COMMENT '角色：1-管理员，2-教师，3-学生',
    real_name VARCHAR(20) COMMENT '真实姓名',
    status TINYINT DEFAULT 1 COMMENT '状态：1-启用，0-禁用',
    last_login_time DATETIME COMMENT '最后登录时间',
    create_time DATETIME DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
    update_time DATETIME DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
    UNIQUE KEY uk_username (username)
) COMMENT '用户信息表';

-- 初始化一些基础数据
INSERT INTO subject (subject_code, subject_name) VALUES
('CHINESE', '语文'),
('MATH', '数学'),
('ENGLISH', '英语'),
('PHYSICS', '物理');

-- 初始化管理员账号
INSERT INTO user (username, password, role, real_name, status) VALUES
('admin', 'e10adc3949ba59abbe56e057f20f883e', 1, '管理员', 1); -- 密码为123456的MD5值