package com.example.user.service;

import com.baomidou.mybatisplus.extension.service.IService;

import com.example.user.entity.User;

//public interface UserService extends IService<User>{
//
//}
public interface UserService {
    User login(String username, String password);

    boolean registerUser(User user);
}