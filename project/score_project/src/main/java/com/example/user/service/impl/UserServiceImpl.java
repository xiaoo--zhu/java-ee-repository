package com.example.user.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.example.user.dao.UserDao;
import com.example.user.entity.User;
import com.example.user.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UserDao userDao;

//    登录
    @Override
    public User login(String username, String password) {
        QueryWrapper<User> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("username", username).eq("password", password);
        return userDao.selectOne(queryWrapper);
    }

//    注册
    @Override
    public boolean registerUser(User user) {
        // 检查学生是否已存在
        QueryWrapper<User> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("username", user.getUsername());
        User existingStudent = userDao.selectOne(queryWrapper);
        if (existingStudent != null) {
            return false; // 学生已存在
        }
        return userDao.insert(user) > 0;
    }
}