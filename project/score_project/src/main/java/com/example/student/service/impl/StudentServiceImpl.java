package com.example.student.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.example.common.PageBean;
import com.example.common.Result;
import com.example.student.dao.StudentDao;
import com.example.student.entity.Student;
import com.example.student.service.StudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
public class StudentServiceImpl extends ServiceImpl<StudentDao, Student> implements StudentService {

    @Autowired
    private StudentDao studentDao;

//    模糊搜索
    @Override
    public List<Student> fuzzySearch(String name) {
        if (name==null) {
            throw new RuntimeException("学生姓名不能为空");
        }
        LambdaQueryWrapper<Student> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.like(Student::getName, name);  // 使用like进行模糊查询
        List<Student> students = this.list(queryWrapper);
        return students;
}
//分页查询
//    @Override
//    public Result<Student> List(PageBean<Student> pageBean) {
//        Page<Student> page = new Page<>(pageBean.getPageNum(), pageBean.getPageSize());
//        QueryWrapper<Student> queryWrapper = new QueryWrapper<>();
//        Student student = pageBean.getDate();
//        if (student!=null&&student.getName()!=null){
//            queryWrapper.like("name",pageBean.getDate().getName());
//        }
//        page.setTotal(getNum());
//        Page<Student> userPage = studentDao.selectPage(page, queryWrapper);
//
//        return Result.success(userPage.getRecords(), userPage.getTotal());
//    }

    //    分页查询
//    @Override
    public Page<Student> findStudentsByPage(Page<Student> page, Student queryStudent) {
        QueryWrapper<Student> queryWrapper = new QueryWrapper<>();
        if (queryStudent != null) {
            if (queryStudent.getName() != null && !queryStudent.getName().isEmpty()) {
                queryWrapper.like("name", queryStudent.getName());
            }
            if (queryStudent.getGender() != null) {
                queryWrapper.eq("gender", queryStudent.getGender());
            }
            // 添加其他查询条件...
        }
//        获取学生表总数
        page.setTotal(getNum());
        return this.page(page, queryWrapper);
    }

//    根据studentId删除信息编辑信息
    @Override
    public boolean updateStudentInfo(Student student) {
        UpdateWrapper<Student> updateWrapper = new UpdateWrapper<>();
        updateWrapper.eq("student_id", student.getStudentId());
        return studentDao.update(student, updateWrapper) > 0;
    }
//    新增信息
    @Override
    public boolean addStudent(Student student) {
        return studentDao.insert(student) > 0;
    }
//    根据studentId删除信息
    @Override
    public boolean removeStudentById(Integer studentId) {
        return studentDao.deleteById(studentId) > 0;
    }

//    计算学生表的总数
    private Long getNum(){
        QueryWrapper<Student> queryWrapper = new QueryWrapper<>();
        return this.count(queryWrapper);
    }
}
