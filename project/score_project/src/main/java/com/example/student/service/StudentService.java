package com.example.student.service;


import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.example.common.PageBean;
import com.example.common.Result;
import com.example.student.entity.Student;

import java.util.List;

public interface StudentService extends IService<Student> {
//    分页查询
    Page<Student> findStudentsByPage(Page<Student> page, Student queryStudent);
//    编辑学生信息
    boolean updateStudentInfo(Student student);
//    新增学生信息
    boolean addStudent(Student student);
//    根据id删除学生信息
    boolean removeStudentById(Integer studentId);

//    模糊搜索
    List<Student> fuzzySearch(String name);

//    分页查询
//    Result<Student> List(PageBean<Student> pageBean);
}
