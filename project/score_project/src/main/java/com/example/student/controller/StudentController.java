package com.example.student.controller;


import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.example.common.PageBean;
import com.example.common.Result;
import com.example.student.entity.Student;
import com.example.student.service.StudentService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/students")
public class StudentController {
    private final StudentService studentService;

    @Autowired
    public StudentController(StudentService studentService) {
        this.studentService = studentService;
    }

    //    模糊搜索
    @PostMapping("/fuzzySearch")
    public List<Student> fuzzySearch(@RequestBody Student student) {
        return studentService.fuzzySearch(student.getName());
    }

    //    分页查询
    @GetMapping
    public Page<Student> getStudents(
            @RequestParam(defaultValue = "1") int page,
            @RequestParam(defaultValue = "10") int size,
            Student student) {
        Page<Student> pageParam = new Page<>(page, size);
        return studentService.findStudentsByPage(pageParam, student);
    }
//    @ApiOperation(value = "分页查询用户", notes = "分页查询用户")
//    @PostMapping("/page")
//    public Result<Student> page(@RequestBody PageBean<Student> pageBean) {
//        return studentService.List(pageBean);
//    }

    //    根据studentId编辑信息
    @PutMapping("/{studentId}")
    public boolean updateStudent(@PathVariable Integer studentId, @RequestBody Student student) {
        student.setStudentId(studentId); // 确保学生ID被设置
        return studentService.updateStudentInfo(student);
    }

    //    添加学生信息
    @PostMapping
    public boolean addStudent(@RequestBody Student student) {
        return studentService.addStudent(student);
    }

    //    根据studentId删除学生信息
    @DeleteMapping("/{studentId}")
    public boolean deleteStudent(@PathVariable Integer studentId) {
        return studentService.removeStudentById(studentId);
    }
}

