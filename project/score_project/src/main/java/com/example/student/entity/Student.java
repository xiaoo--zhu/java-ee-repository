package com.example.student.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.util.Date;

@Data
@TableName("student")
public class Student {
    @TableId(value = "student_id",type = IdType.AUTO)
    private Integer studentId; // 主键ID

    @TableField(value = "student_code")
    private String studentCode; // 学号

    @TableField(value = "name")
    private String name; // 姓名

    @TableField(value = "gender")
    private String gender; // 性别：M-男，F-女

    @TableField(value = "class_id")
    private Integer classId; // 所属班级ID

    @TableField(value = "phone")
    private String phone; // 联系电话

    @TableField(value = "enroll_date")
    private Date enrollDate; // 入学时间

    @TableField(value = "status")
    private Byte status; // 状态：1-在读，2-毕业，3-退学

    @TableField(value = "create_time")
    private Date createTime; // 创建时间

    @TableField(value = "update_time")
    private Date updateTime; // 更新时间
}
