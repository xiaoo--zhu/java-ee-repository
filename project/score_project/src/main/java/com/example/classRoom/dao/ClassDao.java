package com.example.classRoom.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.classRoom.entity.ClassInfo;
import org.apache.ibatis.annotations.Mapper;

@Mapper
// MyBatis Plus已经提供了默认的CRUD操作，包括分页查询
public interface ClassDao extends BaseMapper<ClassInfo> {
    // MyBatis Plus已经提供了分页查询的方法，这里不需要额外定义
}
