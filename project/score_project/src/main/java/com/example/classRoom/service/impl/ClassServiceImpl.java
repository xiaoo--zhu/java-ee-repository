package com.example.classRoom.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.example.classRoom.dao.ClassDao;
import com.example.classRoom.entity.ClassInfo;
import com.example.classRoom.service.ClassService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
public class ClassServiceImpl extends ServiceImpl<ClassDao, ClassInfo> implements ClassService {

    @Autowired
    private ClassDao classDao;
//模糊查询
    @Override
    public List<ClassInfo> fuzzySearch(String className) {
        if (className==null) {
            throw new RuntimeException("班级名称不能为空");
        }
        LambdaQueryWrapper<ClassInfo> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.like(ClassInfo::getClassName, className);  // 使用like进行模糊查询
        List<ClassInfo> classInfo = this.list(queryWrapper);
        return classInfo;
    }

//    分页查询
    @Override
    public Page<ClassInfo> getClassInfoPage(int current, int size) {
        Page<ClassInfo> page = new Page<>(current, size);
        QueryWrapper<ClassInfo> queryWrapper = new QueryWrapper<>();
        page.setTotal(getNum());
        return this.page(page,queryWrapper);// 使用MyBatis-Plus的selectPage方法进行分页查询
    }

//    根据classId删除信息编辑信息
    @Override
    public boolean updateClassInfo(ClassInfo classInfo) {
        UpdateWrapper<ClassInfo> updateWrapper = new UpdateWrapper<>();
        updateWrapper.eq("class_id", classInfo.getClassId());
        return classDao.update(classInfo, updateWrapper) > 0;
    }
//    新增信息
    @Override
    public boolean addClass(ClassInfo classInfo) {
        return classDao.insert(classInfo) > 0;
    }
//    根据classId删除信息
    @Override
    public boolean removeClassById(Integer classId) {
        return classDao.deleteById(classId) > 0;
    }

//    计算学生表的总数
    private Long getNum(){
        QueryWrapper<ClassInfo> queryWrapper = new QueryWrapper<>();
        return this.count(queryWrapper);
    }
}
