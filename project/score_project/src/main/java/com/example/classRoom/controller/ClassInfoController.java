package com.example.classRoom.controller;


import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.example.classRoom.entity.ClassInfo;
import com.example.classRoom.service.ClassService;
import com.example.student.entity.Student;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/classes")
public class ClassInfoController {


    @Autowired
    private ClassService classService;
    //    分页查询
    @GetMapping
    public Page<ClassInfo> getClassInfo(@RequestParam(defaultValue = "1") int page,
                                        @RequestParam(defaultValue = "10") int size) {
        return classService.getClassInfoPage(page, size);
    }
//    根据classId编辑信息
    @PutMapping("/{classId}")
    public boolean updateClass(@PathVariable Integer classId, @RequestBody ClassInfo classInfo) {
        classInfo.setClassId(classId); // 确保学生ID被设置
        return classService.updateClassInfo(classInfo);
    }
//    添加班级信息
    @PostMapping
    public boolean addClass(@RequestBody ClassInfo classInfo) {
        return classService.addClass(classInfo);
    }
//    根据classId删除班级信息
    @DeleteMapping("/{classId}")
    public boolean deleteClass(@PathVariable Integer classId) {
        return classService.removeClassById(classId);
    }
//模糊搜索
    @PostMapping("/fuzzySearch")
    public List<ClassInfo> fuzzySearch(@RequestBody ClassInfo classInfo) {
        return classService.fuzzySearch(classInfo.getClassName());
    }
}

