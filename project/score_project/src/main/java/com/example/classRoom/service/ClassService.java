package com.example.classRoom.service;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.IService;
import com.example.classRoom.entity.ClassInfo;

import java.util.List;

public interface ClassService extends IService<ClassInfo> {
//    分页查询
    Page<ClassInfo> getClassInfoPage(int current, int size);
//    编辑班级信息
    boolean updateClassInfo(ClassInfo classInfo);
//    新增班级信息
    boolean addClass(ClassInfo classInfo);
//    根据id删除班级信息
    boolean removeClassById(Integer classId);
//模糊搜索
    List<ClassInfo> fuzzySearch(String className);

}
