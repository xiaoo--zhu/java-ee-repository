package com.example.classRoom.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.util.Date;

@Data
@TableName("class")
public class ClassInfo {
    @TableId(value = "class_id",type = IdType.AUTO)
    private Integer classId; // 班级ID

    @TableField(value = "class_code")
    private String classCode; // 班级编号

    @TableField(value = "class_name")
    private String className; // 班级名称

    @TableField(value = "teacher_name")
    private String teacherName; // 班主任姓名

    @TableField(value = "student_count")
    private int studentCount; // 学生人数，默认为0

    @TableField(value = "description")
    private String description; // 班级描述

    @TableField(value = "create_time")
    private Date createTime; // 创建时间

    @TableField(value = "update_time")
    private Date updateTime; // 更新时间
}
