package com.example.common;

import lombok.Data;

import java.util.List;

@Data
public class Result<T> {
    private Integer code ;//1 成功 , 0 失败
    private String msg; //提示信息
    private  T data; //数据 data

    private long total; //数据总条数

    public Result() {
    }
    public Result(Integer code, String msg, T data, long total) {
        this.code = code;
        this.msg = msg;
        this.data = data;
        this.total = total;
    }

    public static <E>Result<E> success(E data){
        return new Result(1, "success", data, 1);
    }

    public static <E>Result<E> success(List<E> data, long total){
        return new Result(1, "success", data, total);
    }

    public static <E> Result error(String message){
        return new Result(0, message, null, 0);
    }
}
