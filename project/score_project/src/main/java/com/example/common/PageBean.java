package com.example.common;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class PageBean<T>{

    @ApiModelProperty("当前页")
    private Integer pageNum; //当前页

    @ApiModelProperty("每页显示条数")
    private Integer pageSize; //每页显示条数

    @ApiModelProperty("返回数据")
    private T date;
}

