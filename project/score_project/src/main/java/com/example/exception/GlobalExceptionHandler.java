//package com.example.exception;
//
//import org.springframework.web.bind.annotation.ExceptionHandler;
//import org.springframework.web.bind.annotation.RestControllerAdvice;
//import java.util.HashMap;
//import java.util.Map;
//
//@RestControllerAdvice
//public class GlobalExceptionHandler {
//
//    @ExceptionHandler(BusinessException.class)
//    public Map<String, Object> handleBusinessException(BusinessException e) {
//        Map<String, Object> result = new HashMap<>();
//        result.put("code", e.getCode());
//        result.put("message", e.getMessage());
//        return result;
//    }
//
//    @ExceptionHandler(Exception.class)
//    public Map<String, Object> handleException(Exception e) {
//        Map<String, Object> result = new HashMap<>();
//        result.put("code", 500);
//        result.put("message", "系统内部错误");
//        return result;
//    }
//}