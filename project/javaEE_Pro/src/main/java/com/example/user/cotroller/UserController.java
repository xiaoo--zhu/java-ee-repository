package com.example.user.cotroller;

import com.example.common.PageBean;
import com.example.common.Result;
import com.example.user.entity.User;
import com.example.user.service.UserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Map;


@RestController
@RequestMapping("/users")
@Api(value = " 用户管理", tags = "用户管理")
public class UserController {

    @Autowired
    private UserService userService;

    @ApiOperation(value = "注册用户",notes = "注册用户")
    @PostMapping("/register")
    public Result addUser(@RequestBody User user)  {
       return userService.insert(user);
    }

    @ApiOperation(value = "用户登录",notes = "用户登录")
    @PostMapping("/login")
    public Result login(@RequestBody Map<String, Object> map)  {
        return userService.login(map);
    }

    @ApiOperation(value ="退出登录",notes = "退出登录")
    @GetMapping("/logout")
    public Result logout(@RequestHeader("token") String token) {
        return userService.logout(token);
    }

    @ApiOperation(value = "根据id修改用户",notes = "根据id修改用户")
    @PutMapping("/updateById")
    public Result<User> updateUser(@RequestBody User user) {
       return userService.updateUser(user);
    }

    @ApiOperation(value = "修改密码",notes = "修改密码")
    @PatchMapping("/updatePwd")
    public Result updatePwd(@RequestBody Map<String,Object> map,@RequestHeader("token") String token) {
        return userService.updatePwd(map,token);
    }

    @ApiOperation(value = "根据id删除用户",notes = "根据id删除用户")
    @DeleteMapping("/deleteById")
    public Result deleteById(@RequestBody User user) {
        return userService.deleteById(user);
    }

    @ApiOperation(value = "根据id查询用户",notes = "根据id查询用户")
    @GetMapping("/selectById")
    public Result<User> selectById(@RequestBody User user) {
        return userService.selectById(user.getId());
    }

    @ApiOperation(value = "分页查询用户",notes = "分页查询用户")
    @PostMapping("/page")
    public Result<User> page(@RequestBody PageBean<User> pageBean)  {
        return userService.List(pageBean);
    }

    @ApiOperation(value = "根据id批量删除用户",notes = "根据id批量查询用户")
    @DeleteMapping("/deleteByIds")
    public Result deleteByIds(@RequestBody Map<String, Object> map) {
        return userService.deleteByIds(map);
    }

}
