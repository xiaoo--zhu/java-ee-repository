package com.example.user.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.example.common.Result;
import com.example.user.entity.User;
import com.example.common.PageBean;

import java.util.Map;

public interface UserService extends IService<User> {

    Result login(Map<String, Object> map) ;

    Result insert(User user) ;

    Result<User> updateUser(User user);

    Result deleteById(User user);

    Result<User> selectById(Integer id);

    Result deleteByIds(Map<String, Object> map) ;

    Result updatePwd(Map<String, Object> map, String token);

    Result logout(String token);

    Result<User> List(PageBean<User> pageBean);
}
