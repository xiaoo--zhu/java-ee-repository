package com.example.user.service.impl;


import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.LambdaUpdateWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.example.common.PageBean;
import com.example.common.Result;
import com.example.user.dao.UserDao;
import com.example.user.entity.User;
import com.example.user.service.UserService;
import com.example.utils.JwtUtils;
import com.example.utils.Md5Util;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

@Service
public class UserServiceImpl extends ServiceImpl<UserDao, User> implements UserService {

    @Autowired
    private UserDao userDao;

    @Autowired
    private UserService userService;

    public Result insert(User user) {
        QueryWrapper<User> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("username", user.getUsername());
        if (userService.getOne(queryWrapper) != null) {
            return Result.error("用户名已存在");
        }

        String md5string = Md5Util.getMD5String(user.getPassword());
        user.setPassword(md5string);
        userService.save(user);
        return Result.success(user.getId());
    }


    public Result updateUser(User user) {
        Integer userId = user.getId();
        if (userId == null) {
            return Result.error("用户id不能为空");
        }
        User currentUser = userService.getById(userId);
        if (currentUser == null) {
            return Result.error("用户不存在");
        }
        if (user.getPassword()!=null&&!currentUser.getPassword().equals(user.getPassword())) {
            return Result.error("密码不允许修改");
        }
        user.setPassword(currentUser.getPassword());
        QueryWrapper<User> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("id", user.getId());
        if (userService.getOne(queryWrapper) == null) {
            return Result.error("用户id不存在");
        }
        user.setPassword(currentUser.getPassword());//保留原密码
        userService.updateById(user);
        return Result.success("修改成功");
    }

    public Result deleteById(User user) {
        Integer userId = user.getId();
        if (userId == null) {
            return Result.error("用户id不能为空");
        }else {
            QueryWrapper<User> queryWrapper = new QueryWrapper<>();
            queryWrapper.eq("id", user.getId());
            if (userService.getOne(queryWrapper) == null) {
                return Result.error("用户id不存在");
            }
        }
        userService.removeById(user.getId());
        return Result.success("删除成功");
    }


    public Result<User> selectById(Integer id) {
        if (id == null) {
            return Result.error("用户id不能为空");
        }
        LambdaQueryWrapper<User> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(User::getId, id);
        User user = userService.getOne(queryWrapper);
        if (ObjectUtil.isEmpty(user)){
            return Result.error("用户id不存在");
        }
        return Result.success(userService.getById(id));
    }


    public Result<String> login(Map<String, Object> map) {
        String username = map.get("username").toString();
        String password = map.get("password").toString();
        if (StrUtil.isNotEmpty(username) && StrUtil.isNotEmpty(password)) {
            LambdaQueryWrapper<User> queryWrapper = new LambdaQueryWrapper<>();
            queryWrapper.eq(User::getUsername, username);
            User user = getOne(queryWrapper);
            if (ObjectUtil.isEmpty(user)) {
                return Result.error("用户不存在");
            } else {
                if (Md5Util.getMD5String(password).equals(user.getPassword())) {
                    Map<String,Object> claims = new HashMap<>();
                    claims.put("id",user.getId());
                    claims.put("username",user.getUsername());
                    String token = JwtUtils.generateToken(claims.toString());
                    return Result.success(token);
                }
            }
        }
        return Result.error("登录错误");
    }

    @Override
    public Result<User> List(PageBean<User> pageBean) {
        Page<User> page = new Page<>(pageBean.getPageNum(), pageBean.getPageSize());
        System.out.println("pageBean.getPageNum() = " + pageBean.getPageNum());
        QueryWrapper<User> queryWrapper = new QueryWrapper<>();
        User user = pageBean.getDate();
        if (user!=null&&user.getName()!=null){
            queryWrapper.like("name",pageBean.getDate().getName());
        }
        Page<User> userPage = userDao.selectPage(page, queryWrapper);

        return Result.success(userPage.getRecords(), userPage.getTotal());
    }


    public Result deleteByIds(Map<String, Object> map) {
        String ids = map.get("ids").toString();
        List<String> idList = Arrays.asList(ids.split(","));
        userService.removeByIds(idList);
        return Result.success("删除成功");
    }


    public Result updatePwd(Map<String, Object> map, String token) {
        String oldPwd =  map.get("oldPwd").toString();
        String newPwd = map.get("newPwd").toString();
        String rePwd = map.get("rePwd").toString();
        if (StrUtil.isEmpty(oldPwd) || StrUtil.isEmpty(newPwd) || StrUtil.isEmpty(rePwd)) {
            return Result.error("缺少必要参数");
        }
        String username = map.get("username").toString();
        User user = userDao.selectOne(new QueryWrapper<User>().eq("username", username));
        if (user == null) {
            return Result.error("用户不存在");
        }
        if (!user.getPassword().equals(Md5Util.getMD5String(oldPwd))) {
            return Result.error("原密码填写不正确");
        }

        if (!newPwd.equals(rePwd)) {
            return Result.error("两次填写密码不一致");
        }
        try {
            updatepwd(user.getId(), newPwd);
        } catch (Exception e) {
            log.error("更新密码失败", e);
            return Result.error("修改失败，请稍后再试");
        }
        // 返回成功消息，并提示重新登录
        return Result.success("密码修改成功，请重新登录");
    }

    public Result logout(String token) {
        return Result.success("退出成功");
    }

    public void updatepwd(Integer userId, String newPwd) {
        LambdaUpdateWrapper<User> lambdaUpdateWrapper = new LambdaUpdateWrapper<>();
        lambdaUpdateWrapper.eq(User::getId, userId)
                .set(User::getPassword, Md5Util.getMD5String(newPwd));
        userDao.update(null, lambdaUpdateWrapper);
    }
 }
