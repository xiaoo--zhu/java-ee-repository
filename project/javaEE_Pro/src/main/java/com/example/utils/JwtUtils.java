package com.example.utils;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class JwtUtils {

    private static final String SECRET_KEY = "myKey_-_"; // 密钥，实际使用应该保密

    // 生产JWT
    public static String generateToken(String stuNumber) {
        Map<String, Object> claims = new HashMap<String, Object>();
        claims.put("stuNumber", stuNumber); //将用户账号作为JWT的主体
        return Jwts.builder()
                .setClaims(claims)
                .setIssuedAt(new Date())  // 发行时间
                .setExpiration(new Date(System.currentTimeMillis()+1000*60*60*10)) // 设置有效时间10个小时
                .signWith(SignatureAlgorithm.HS256,SECRET_KEY)  //设置签名算法和密钥
                .compact();
    }

    // 解析JWT
    public static Claims parseToken(String token) {
        return Jwts.parser()
                .setSigningKey(SECRET_KEY)
                .parseClaimsJws(token)
                .getBody();
    }

    // 解析并验证JWT,h获取其载荷
    public static Claims validateToken(String token) {
        Claims claims = parseToken(token);
        Date now = new Date();
        Date expiration = claims.getExpiration();
        if(expiration.before(now)){
            throw new RuntimeException("Token expired");
        }
        return claims;
    }
}