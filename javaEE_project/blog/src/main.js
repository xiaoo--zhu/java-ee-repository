import './assets/main.css'

import { createApp } from 'vue'
import App from './App.vue'
//1 导入vue-router
import { createRouter, createWebHistory } from 'vue-router';
//3 导入组件
import BlogList from "@/components/BlogList.vue";
//2 配置路由
const routes = [
    {
        path: '/blogs',
        name: 'Blogs',
        component: BlogList,
    }
];
//4 创建路由实例
const router = createRouter({
    history: createWebHistory(),
    routes,
});
// 创建vue应用
const app = createApp(App);
// 5 使用路由
app.use(router);
app.mount('#app');
