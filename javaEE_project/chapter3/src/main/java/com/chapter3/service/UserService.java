package com.chapter3.service;

import com.chapter3.entity.User;

public interface UserService {
    int addUser(User user);
    int updateUser(User user);
    int deleteUser(int id);
    User findUser(int id);
}
