package com.chapter3.service;

public interface LoginService {
    boolean login(String username,String password);
}
