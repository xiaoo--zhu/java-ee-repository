package com.chapter3.service.iml;

import com.chapter3.entity.User;
import com.chapter3.service.UserService;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

@Service
public class UserServiceIml implements UserService {
    JdbcTemplate jdbcTemplate;
    public UserServiceIml(JdbcTemplate jdbcTemplate){
        this.jdbcTemplate=jdbcTemplate;
    }
    @Override
    public int addUser(User user){
        String sql="insert into user(username,password,sex) values(?,?,?)";
        return jdbcTemplate.update(sql,user.getUsername(),user.getPassword(),user.getSex());
    }

    @Override
    public int updateUser(User user) {
        return 0;
    }

    @Override
    public int deleteUser(int id) {
        return 0;
    }

    @Override
    public User findUser(int id) {
        return null;
    }
}
