package com.chapter3.entity;

import lombok.Data;

@Data
public class User {
    int id;
    String username;
    String password;
    String sex;
}

