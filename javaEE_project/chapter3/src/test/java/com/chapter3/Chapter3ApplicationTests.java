package com.chapter3;

import com.chapter3.entity.User;
import com.chapter3.service.LoginService;
import com.chapter3.service.UserService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class Chapter3ApplicationTests {
    @Autowired
    private UserService userService;
    @Test
    void contextLoads() {
        User user =new User();
        user.setSex("男");
        user.setUsername("zxm");
        user.setPassword("123456");
        userService.addUser(user);
    }

    @Autowired
    private LoginService loginService;
    @Test
    void text(){
        loginService.login("zxm","123456");
    }

}
