package com.example.chapter5.service;

import com.example.chapter5.entity.Student;

public interface StudentService {
    Student findById(Integer id);
    String add(Student student);

    String update(Student student);

    String delete(Integer id);
}
