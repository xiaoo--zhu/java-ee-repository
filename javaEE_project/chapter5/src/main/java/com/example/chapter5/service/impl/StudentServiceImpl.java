package com.example.chapter5.service.impl;

import com.example.chapter5.entity.Student;
import com.example.chapter5.mapper.StudentMapper;
import com.example.chapter5.service.StudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class StudentServiceImpl implements StudentService {
    @Autowired
    private StudentMapper studentMapper;
    @Override
    public Student findById(Integer id) {
        Student student = studentMapper.findById(id);
        return student;
    }

    @Override
    public String add(Student student) {
        int add = studentMapper.add(student);
        if (add == 1){
            return "添加成功";
        }return "添加失败";
    }
    @Override
    public String update(Student student) {
        int update = studentMapper.update(student);
        if (update == 1){
            return "修改成功";
        }return "修改失败";
    }
    @Override
    public String delete(Integer id) {
        int delete = studentMapper.delete(id);
        if (delete == 1){
            return "删除成功";
        }return "删除失败";
    }
}
