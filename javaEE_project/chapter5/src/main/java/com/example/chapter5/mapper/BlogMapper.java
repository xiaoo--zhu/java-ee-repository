package com.example.chapter5.mapper;

import com.example.chapter5.entity.Blog;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import java.util.List;

@Mapper
public interface BlogMapper {
    //查找数据库里面所有的博客数据
//    @Select("select * from m_blog")
//    List<Blog> selectAll();
//    //查询分页数据,跳过前 offset行，返回接下来的 limit 行数据
//    @Select("select * from m_blog order by created_at desc limit #{limit} offset #{offset}")
//    List<Blog> findBlogsByPage(int offset, int limit);
//    //查询博客总数量
//    @Select("select count(*) from m_blog")
//    int countBlogs();

    //获取所有博客
    @Select("select * from m_blog ORDER BY created_at DESC ")
    List<Blog> findAll();
    //获取博客总数量
    @Select("select count(*) from m_blog")
    int countBlogs();
    //分页查询,跳过offset行，返回接下来的limit行
    @Select("select * from m_blog ORDER BY created_at DESC LIMIT #{limit} OFFSET #{offset}")
    List<Blog> findBlogsByPage(int offset, int limit);
    // 删除博客
    @Delete("DELETE FROM m_blog WHERE id = #{id}")
    void deleteBlog(int id);
    // 更新博客
    @Update("UPDATE m_blog SET title = #{title}, description = #{description}, " +
            "content = #{content}, status = #{status}, created_at = NOW() WHERE id = #{id}")
    void updateBlog(Blog blog);
}
