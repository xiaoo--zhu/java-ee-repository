package com.example.chapter5.service;

import com.example.chapter5.entity.Blog;

import java.util.List;

public interface BlogService {
//    List<Blog> findAll();
//    List<Blog> findByPage(int page, int size);
//    int count();
List<Blog> findAll();
    List<Blog> findBlogsByPage(int page, int pageSize);
    int countBlogs();
    void deleteBlog(int id);
    void updateBlog(Blog blog);
}
