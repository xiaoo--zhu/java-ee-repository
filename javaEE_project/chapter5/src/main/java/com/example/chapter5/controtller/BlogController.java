package com.example.chapter5.controtller;

import com.example.chapter5.entity.Blog;
import com.example.chapter5.service.BlogService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@CrossOrigin(origins = "http://localhost:5173")
@RestController
public class BlogController {
    @Autowired
    private BlogService blogService;
    //显示所有博客内容
//    @GetMapping("/api/blogs")
//    public List<Blog> getBlogs() {
//        List<Blog> blogs = new ArrayList<Blog>();
//        //调用Service获取所有博客的内容
//        return blogService.findAll();
//    }
    //分页显示博客内容，通过offset limit来进行选择性查询
    //处理分页参数，通过url传递分页信息（page和size），并且设置默认值
//    @GetMapping("/api/blogs")
//    public ResponseEntity getBlogsByPage(
//            @RequestParam(defaultValue = "1") int page,
//            @RequestParam(defaultValue = "10") int size) {
//        //获取当前页面的数据
//        List<Blog> blogs = blogService.findByPage( page,size);
//        //获取总博客数量
//        int total = blogService.count();
//        //构建相应数据
//        Map<String,Object> map = new HashMap<>();
//        map.put("blogs",blogs);
//        map.put("total",total);
//        return ResponseEntity.ok(map);
//    }

    @GetMapping("/api/blogs")
    public ResponseEntity getBlogsByPage(
            @RequestParam(defaultValue = "1") int page,
            @RequestParam(defaultValue = "10")int size) {
        List<Blog> blogs = blogService.findBlogsByPage(page, size);
        int count = blogService.countBlogs();
        Map<String,Object> map = new HashMap<>();
        map.put("blogs", blogs);
        map.put("count", count);
        return ResponseEntity.ok(map);
    }

    @DeleteMapping("/api/blogs/{id}")
    public ResponseEntity<?> deleteBlog(@PathVariable("id") int id) {
        try {
            blogService.deleteBlog(id);
            return ResponseEntity.ok().build();
        } catch (Exception e) {
            return ResponseEntity.badRequest().body("删除失败：" + e.getMessage());
        }
    }
    @PutMapping("/api/blogs/{id}")
    public ResponseEntity<?> updateBlog(@PathVariable("id") int id, @RequestBody Blog blog) {
        try {
            blog.setId(id);
            blogService.updateBlog(blog);
            return ResponseEntity.ok(blog);
        } catch (Exception e) {
            return ResponseEntity.badRequest().body("更新失败：" + e.getMessage());
        }
    }
}
