package com.example.chapter5.service.impl;

import com.example.chapter5.entity.Blog;
import com.example.chapter5.mapper.BlogMapper;
import com.example.chapter5.service.BlogService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class BlogServiceIml implements BlogService {
//    @Autowired
//    BlogMapper blogMapper;
//    @Override
//    public List<Blog> findAll() {
//        //查找所有博客，通过调用mapper对象的方法来实现
//        return blogMapper.selectAll();
//    }
//
//    @Override
//    public List<Blog> findByPage(int page, int size) {
//        //查询分页数据
//        //计算从第几行开始查询
//        int offset = (page - 1) * size;
//        return blogMapper.findBlogsByPage(offset, size);
//    }
//    //查询博客数量
//    @Override
//    public int count() {
//        return blogMapper.countBlogs();
//    }

    @Autowired
    BlogMapper blogMapper;
    @Override
    public List<Blog> findAll() {
        //调用Mapper，查询所有博客的数据
        return blogMapper.findAll();
    }
    //分页查询
    @Override
    public List<Blog> findBlogsByPage(int page, int pageSize) {
        //计算offset
        int offset = (page - 1) * pageSize;
        return blogMapper.findBlogsByPage(offset, pageSize);
    }
    //获取博客数量
    @Override
    public int countBlogs() {
        return blogMapper.countBlogs();
    }
    // 实现删除方法
    @Override
    public void deleteBlog(int id) {
        blogMapper.deleteBlog(id);
    }
    // 实现更新方法
    @Override
    public void updateBlog(Blog blog) {
        blogMapper.updateBlog(blog);
    }
}
