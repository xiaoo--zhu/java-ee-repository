package com.example.chapter5.entity;

import lombok.Data;

@Data
public class Student {
    private Integer id;
    private String name;
    private String qq;
}

