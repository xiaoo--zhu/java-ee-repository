package com.example.chapter5.mapper;

import com.example.chapter5.entity.Student;
import org.apache.ibatis.annotations.*;

@Mapper
public interface StudentMapper {
//    根据id查询学生信息
    @Select("select * from student where id = #{id}")
//    Student getStudentById(Integer id);
    Student findById(Integer id);

    @Insert("insert into student(qq, name) values(#{qq}, #{name})")
    int add(Student student);

    @Update("update student set qq = #{qq}, name = #{name} where id = #{id}")
    int update(Student student);

    @Delete("delete from student where id = #{id}")
    int delete(Integer id);
}
