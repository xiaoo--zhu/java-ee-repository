package com.example.chapter5.config;


import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configurers.AbstractHttpConfigurer;
import org.springframework.security.web.SecurityFilterChain;

@Configuration
@EnableWebSecurity
public class SecurityConfiguration {
    @Bean
    public SecurityFilterChain filterChain(HttpSecurity http) throws Exception {
        //Spring Security 中配置 HTTP 请求授权规则的方法。
        http.authorizeHttpRequests(
                        (authorizeHttpRequests) -> authorizeHttpRequests
                                //用于指定一组 URL,任何用户都可以访问，无需经过认证。
                                .requestMatchers("/", "/login", "/register").permitAll()
                                //anyRequest() 代表所有未特别指定的请求，而 authenticated() 则要求用户必须通过认证。
                                .anyRequest().authenticated()
                )
                //禁用
                .csrf(AbstractHttpConfigurer::disable);
        //用于构造并返回配置好的 SecurityFilterChain
        return http.build();
//                .addFilterBefore(new JwtAuthenticationFilter(), UsernamePasswordAuthenticationFilter.class)
//                .logout((logout) ->
//                                logout
//                                        .logoutUrl("/logout")
//                                        .logoutSuccessUrl("/logout/success"));
//        return http.build();
    }
}