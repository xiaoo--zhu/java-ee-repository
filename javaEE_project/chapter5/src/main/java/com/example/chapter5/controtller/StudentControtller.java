package com.example.chapter5.controtller;

import com.example.chapter5.entity.Student;
import com.example.chapter5.service.StudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

@RestController
public class StudentControtller {
    @Autowired
    private StudentService studentService;
    @PostMapping(value="/students/{id}")
    public Object student(@PathVariable("id") Integer id){
        return studentService.findById(id);
    }
    @PostMapping(value = "/add")
    public String add(@RequestBody  Student student) {
        return studentService.add(student);
    }
    @PostMapping(value = "/update")
    @ResponseBody
    public String update(@RequestBody  Student student) {
        return studentService.update(student);
    }
    @DeleteMapping(value = "/delete/{id}")
    public String delete(@PathVariable Integer id) {
        return studentService.delete(id);
    }
}
