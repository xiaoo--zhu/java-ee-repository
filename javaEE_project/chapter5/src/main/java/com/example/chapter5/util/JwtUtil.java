package com.example.chapter5.util;

import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTVerifier;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.exceptions.JWTVerificationException;
import com.auth0.jwt.interfaces.Claim;
import com.auth0.jwt.interfaces.DecodedJWT;
import com.example.chapter5.entity.Student;
import org.springframework.security.core.userdetails.User;

import java.util.Calendar;
import java.util.Date;
import java.util.Map;

public class JwtUtil {
    //Jwt秘钥
    private static final String key = "abcdefghijklmn";

    //根据用户信息创建Jwt令牌
    public static String createJwt(Student user){
        Algorithm algorithm = Algorithm.HMAC256(key);
        Calendar calendar = Calendar.getInstance();
        Date now = calendar.getTime();
        calendar.add(Calendar.SECOND, 3600 * 24 * 7);
        return JWT.create()
                .withClaim("name", user.getName())  //配置JWT自定义信息
                .withExpiresAt(calendar.getTime())  //设置过期时间
                .withIssuedAt(now)    //设置创建创建时间
                .sign(algorithm);   //最终签名
    }

    //根据Jwt验证并解析用户信息
    public static Map<String, Claim> resolveJwt(String token){
        Algorithm algorithm = Algorithm.HMAC256(key);
        //解析token数据
        JWTVerifier jwtVerifier = JWT.require(algorithm).build();
        try {
            DecodedJWT verify = jwtVerifier.verify(token);  //对JWT令牌进行验证，看看是否被修改
            Map<String, Claim> claims = verify.getClaims();  //获取令牌中内容
            if(new Date().after(claims.get("exp").asDate())) //如果是过期令牌则返回null
                return null;
            else
                return claims;
        } catch (JWTVerificationException e) {
            return null;
        }
    }
}