package com.example.javaee_project;

import com.service.LoginService;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@Slf4j
@SpringBootTest
class JavaEeProjectApplicationTests {
    @Autowired
    private LoginService loginService;

    @Test
    void contextLoads() {
        log.trace("trace");
        log.debug("debug");
        log.info("info");
        log.warn("warn");
        log.error("error");
    }

    @Test
    void testLoginService() {
        assert loginService.login("张三","123");//可以不用管具体是哪个类实现了LoginService
    }






}
