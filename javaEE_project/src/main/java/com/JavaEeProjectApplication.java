package com;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;
import com.service.LoginService;

@SpringBootApplication
public class JavaEeProjectApplication {

    public static void main(String[] args) {
        ConfigurableApplicationContext context= SpringApplication.run(JavaEeProjectApplication.class, args);
        context.getBean(LoginService.class).login("admin","admin");
    }

}
