package com.chapter3_3;

import com.chapter3_3.dao.UserDao;
import com.chapter3_3.entity.User;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

@Slf4j
@SpringBootTest
class Chapter33ApplicationTests {

    @Autowired
    UserDao userDao;
    @Test
    void contextLoads() {

        log.info(String.valueOf(userDao.getUserByName("zhangsan")));
    }

    @Test
    void getUserAndNo(){
        List<User> zhangsan = userDao.getStuByNameOrNo("uygiuyfgiuytg", 1234567890);
        System.out.println(zhangsan);
    }

    @Test
    public void USerTest()
    {
        User zhangsan = userDao.getUserByName("zhangsan");
        System.out.println(zhangsan.toString());
    }
}
