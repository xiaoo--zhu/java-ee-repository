package com.chapter3_3.service.iml;


import com.chapter3_3.dao.UserDao;
import com.chapter3_3.entity.User;
import com.chapter3_3.service.LoginService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Slf4j
@Service
public class LoginServceIml implements LoginService {
    @Autowired
    private UserDao userDao;
    @Override
    public boolean login(String username, String password) {
        User user =userDao.getUserByName(username);
        if(user==null){
            log.info("用户名密码无效");
            return false;
        }else if(user.getPassword().equals(password)){
            log.info("登录成功");
            return true;
        }
        log.info("登录失败");
        return false;
    }
}
