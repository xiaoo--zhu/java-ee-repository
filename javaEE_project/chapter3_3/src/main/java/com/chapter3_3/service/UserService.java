package com.chapter3_3.service;

import com.chapter3_3.entity.User;

public interface UserService {
    int getUserCountAndRegister(User user);
}
