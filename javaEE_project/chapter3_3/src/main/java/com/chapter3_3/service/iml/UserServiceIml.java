package com.chapter3_3.service.iml;

import com.chapter3_3.dao.UserDao;
import com.chapter3_3.entity.User;
import com.chapter3_3.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserServiceIml implements UserService{
    @Autowired
    private UserDao userDao;

    @Override
    public int getUserCountAndRegister(User user) {
        //账号在数据库是否存在
        if (userDao.findUserByName(user)!=0){
            //如果账号已存在抛出异常
            throw new RuntimeException("当前用户已存在");
        }
        //如果账号不存在，添加数据到数据库中并返回
        return userDao.creatUser(user);
    }
}

