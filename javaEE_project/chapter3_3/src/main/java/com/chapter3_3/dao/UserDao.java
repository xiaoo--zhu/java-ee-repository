package com.chapter3_3.dao;

import com.chapter3_3.entity.Card;
import com.chapter3_3.entity.User;
import org.apache.ibatis.annotations.*;

import java.util.List;

@Mapper
public interface UserDao {

//    @Results(id="userMap",value={
//            @Result(property="name",column = "username")
//    })
    @Select("select * from user where username=#{username}")
    User getUserByName(String username);

    // 模糊查询学生名字，并且关联查询校园卡信息
    @Results({
            @Result(property = "card", column = "number", one = @One(select = "getCardByNumber"))
    })
    @Select({
            "<script>",
            "SELECT u.*, c.* ",
            "FROM user u ",
            "LEFT JOIN card c ON u.number = c.cardNo ",
            "WHERE 1=1 ",
            "<if test='name != null and name.trim() != \"\"'>",
            "AND u.username LIKE CONCAT('%', #{name}, '%')", // 更新列名为username
            "</if>",
            "<if test='number != null and number != 0'>",
            "OR u.number = #{number}",
            "</if>",
            "</script>"
    })
    List<User> getStuByNameOrNo(@Param("name") String name, @Param("number") Integer number);

    // 根据卡号查询校园卡信息
    @Select("SELECT * FROM card WHERE cardNo = #{number}")
    Card getCardByNumber(@Param("number") Integer number);

    @Select("select count(1) from user where username = #{username}")
    int findUserByName(User user);

    @Insert("insert into user(username,password,sex) values (#{username},#{password},#{sex})")
    int creatUser(User user);
}


