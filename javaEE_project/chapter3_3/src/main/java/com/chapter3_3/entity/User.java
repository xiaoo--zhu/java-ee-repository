package com.chapter3_3.entity;

import lombok.Data;

@Data
public class User {
    private String id;
    private String username;
    private String password;
    private String sex;
    private Integer number;
}



