package com.chapter3_3.entity;

import lombok.Data;

@Data
public class Card {
    private String cardNo;
}
