package com.javaee_word;

import com.javaee_word.dao.UserDao;
import com.javaee_word.entity.User;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@Slf4j
@SpringBootTest
class JavaEeWordApplicationTests {
    @Autowired
    UserDao userDao;

    @Test
    void contextLoads() {
        User stu=new User();
        stu.setName("王五");
        stu.setSex("女");
        stu.setAge("31");
        userDao.create(stu);
        log.info(stu.toString());
        log.info("新增成功");
    }

    @Test
    void SelectUser() {
//   userDao.getByName("王五");
   log.info(userDao.getByName("王五").toString());
   log.info("查询成功");
    }

    @Test
    void UpdateUser() {
        User stu1=new User();
        stu1.setId(2);
        stu1.setName("王五123");
        stu1.setSex("女");
        userDao.updateByName(stu1);
        log.info(stu1.toString());
        log.info("修改成功");
    }

    @Test
    void deleteUser() {
        userDao.deleteByName("王五123");
        log.info("删除成功");
    }


}
