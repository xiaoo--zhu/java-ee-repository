package com.javaee_word;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class JavaEeWordApplication {

    public static void main(String[] args) {
        SpringApplication.run(JavaEeWordApplication.class, args);
    }

}
