package com.javaee_word.entity;

import lombok.Data;

@Data
public class User {
    int id;
    String name;
    String sex;
    String age;
}
