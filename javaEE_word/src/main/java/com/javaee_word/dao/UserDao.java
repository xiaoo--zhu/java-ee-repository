package com.javaee_word.dao;

import com.javaee_word.entity.User;
import org.apache.ibatis.annotations.*;

import java.util.List;

@Mapper
public interface UserDao {
//    新增
    @Insert("insert into student(name,sex,age) values (#{name},#{sex},#{age})")
    int create(User user);

//    查询
    @Select("select * from student where name=#{name}")
    List<User> getByName(String name);

//    删除
    @Delete("delete from student where name=#{name}")
    int deleteByName(String name);

//    修改信息
    @Update("update student set name=#{name},sex=#{sex},age=#{age} where id=#{id}")
    int updateByName(User user);
}
